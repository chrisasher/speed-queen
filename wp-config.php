<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'speed-queen' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'r@|n,^]*w+V#Ah)2_y!)J}-L8AA!5H$uwB%[]_qWUs7k-#|3-dQuLj2J8*`D=cd1' );
define( 'SECURE_AUTH_KEY',  'DD/h{z +2<P %i]dwF,`QG;=+~C?<:RLBXlo{VeXu-x{]1 SKjlZv&3Z5g~b@1rg' );
define( 'LOGGED_IN_KEY',    'VQ1:b|-P,BtANftTOJ2&],)G0d5{U:}-8d&DE{7C3Jm EPANtk`+<;57U.0>~ @m' );
define( 'NONCE_KEY',        '+~^el92MIM}H*ok$e=(KlZ{v/**[N`UR7TY.Z)LfT7_Y_A<i!t,5@4UJcL+I,/)>' );
define( 'AUTH_SALT',        '8DH?`Hjg.RwFd Q8Y/AmFY&W>pKmN+0dP~iS_&5Mc`TqLtqQ/WRi:8DduCq>p+_D' );
define( 'SECURE_AUTH_SALT', 'v9owhXxxUCD(n]Lz#YLCE$PQNo:? 2Q}!V@G9WBl=L[cdD/{NFbk1lHnMt!2ZGZ&' );
define( 'LOGGED_IN_SALT',   'RaOIyuNTA{?^<|76p]cI]YO2Y^c,MqQ@I_O1nNwT4iIAEhDRV6TEkV__GFcQ>*KW' );
define( 'NONCE_SALT',       ':wEBiL(*!yfVl^fF[G0w9_8^6O69{gq@Y@^}H6t}}5rlG2#I#64O6NL%U.nGn8~/' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'sq_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
