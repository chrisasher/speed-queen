<section id="footer" class="footer-columns py-6 mx-auto flex justify-center">
    <div class="container mx-auto flex flex-col md:flex-row justify-center items-baseline">
        <div class="w-full md:w-1/2 mr-auto contact-details">
            <h3>Contact Details</h3>
            <p><span>Address </span><a href="https://www.google.com/maps/search/<?php if(get_theme_mod('office_street_address')) : echo get_theme_mod('office_street_address'); endif; ?>" target="_blank"><?php if(get_theme_mod('office_street_address')) : echo get_theme_mod('office_street_address'); endif; ?></a></p>
            <p><span>Phone </span><a href="tel:<?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?>"><?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?></a></p>
            <p><span>Email </span><a href="mailto:<?php if(get_theme_mod('contact_email')) : echo get_theme_mod('contact_email'); endif; ?>"><?php if(get_theme_mod('contact_email')) : echo get_theme_mod('contact_email'); endif; ?></a></p>
            <img src="<?php if(get_theme_mod('footer_logo')) : echo get_theme_mod('footer_logo'); endif; ?>" alt="" class="mb-5">
        </div>
        <div class="flex flex-wrap w-full md:w-1/2">
            <div class="menu-links w-1/2">
                <?php dynamic_sidebar( 'footer-services-2' ); ?>
            </div>
            <div class="office w-1/2 items-baseline supplier-icons">
                <?php dynamic_sidebar( 'footer-services-3' ); ?>
            </div>
        </div>
    </div>
</section>
<div class="copyright text-center py-3">
    <p><?php if(get_theme_mod('copyright_details')) : echo get_theme_mod('copyright_details'); endif; ?><?php echo date("Y"); ?> <?php if(get_theme_mod('copyright_after')) : echo get_theme_mod('copyright_after'); endif; ?></p>
</div>
<?php wp_footer(); ?>
</body>
</html>