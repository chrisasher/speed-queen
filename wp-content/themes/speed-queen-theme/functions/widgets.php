<?php
/**
 * Registers a widget area.
 *
 * @link https://developer.wordpress.org/reference/functions/register_sidebar/
 *
 * @since Twenty Sixteen 1.0
 */
function theme_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Widget', 'theme' ),
		'id'            => 'widget',
		'description'   => __( 'Appears on ', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widgettitle how-to-book-3">',
		'after_title'   => '</h3>',
	) );


}
add_action( 'widgets_init', 'theme_widgets_init' );


class jpen_Example_Widget extends WP_Widget {


  // Set up the widget name and description.
  public function __construct() {
    $widget_options = array( 'classname' => 'example_widget', 'description' => 'This is an Example Widget' );
    parent::__construct( 'example_widget', 'Example Widget', $widget_options );
  }


  // Create the widget output.
  public function widget( $args, $instance ) {
	if ( ! isset( $args['widget_id'] ) ) {
        $args['widget_id'] = $this->id;
	}

	// widget ID with prefix for use in ACF API functions
	$widget_id = 'widget_' . $args['widget_id'];


    $title = apply_filters( 'widget_title', $instance[ 'title' ] );
    $blog_title = get_bloginfo( 'name' );
	$tagline = get_bloginfo( 'description' );
	
	echo '<div class="accordion-wrap">';


	echo $args['before_widget'];
	echo '<button class="accordion flex justify-between w-full">';
		echo '<span>'. $title . '</span><span class="plus-minus"></span>';
	echo '</button>';	

	echo '<div class="panel">';


		if( have_rows('filters', $widget_id) ):
			echo '<div>';
			while( have_rows('filters', $widget_id) ) : the_row();
				$shortcode = get_sub_field('shortcode', $widget_id);
				echo do_shortcode($shortcode);
			endwhile;
			echo '</div>';
		endif;
	echo $args['after_widget'];
	echo '</div>';	
	echo '</div>';	
}

  // Create the admin area widget settings form.
  public function form( $instance ) {
    $title = ! empty( $instance['title'] ) ? $instance['title'] : ''; ?>
    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>">Title:</label>
      <input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>" />
    </p><?php
  }


  // Apply settings to the widget instance.
  public function update( $new_instance, $old_instance ) {
    $instance = $old_instance;
    $instance[ 'title' ] = strip_tags( $new_instance[ 'title' ] );
    return $instance;
  }

}

// Register the widget.
function jpen_register_example_widget() { 
  register_widget( 'jpen_Example_Widget' );
}
add_action( 'widgets_init', 'jpen_register_example_widget' );



