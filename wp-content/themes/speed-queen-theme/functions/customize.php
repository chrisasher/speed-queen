<?php
function theme_customize_register( $wp_customize ) {
	// Site settings
	$wp_customize->add_section( 'theme_settings' , array(
		'title'      => __( 'Site Settings', 'theme' ),
		'priority'   => 30,
	) );
	// Logo image
	$wp_customize->add_setting('logo_image', array(
	));
	$wp_customize->add_control( new WP_Customize_Image_Control(
		$wp_customize, 'logo_image', array(
			'label' => __('Site Logo', 'theme'),
			'section'   => 'theme_settings',
			'settings'  => 'logo_image',
		)
	));
	// Footer Logo
	$wp_customize->add_setting('footer_logo', array(
	));
	$wp_customize->add_control( new WP_Customize_Image_Control(
		$wp_customize, 'footer_logo', array(
			'label' => __('Footer Logo', 'theme'),
			'section'   => 'theme_settings',
			'settings'  => 'footer_logo',
		)
	));


	/*
	 * social links start
	 */
	$wp_customize->add_section( 'theme_social_links' , array(
		'title'      => __( 'Social Links', 'theme' ),
		'priority'   => 30,
	) );
	// facebook
	$wp_customize->add_setting( 'facebook_link' , array(
		'default' => 'http://',
	) );
	//twitter
	$wp_customize->add_setting( 'twitter_link' , array(
		'default' => 'http://',
	) );
	// instagram
	$wp_customize->add_setting( 'instagram_link' , array(
		'default' => 'http://',
	) );
	// youtube
	$wp_customize->add_setting( 'youtube_link' , array(
		'default' => 'http://',
	) );

	$wp_customize->add_control(
		'facebook_link', array(
			'label' => __('Facebook Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'facebook_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'twitter_link', array(
			'label' => __('Twitter Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'twitter_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'instagram_link', array(
			'label' => __('Instagram Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'instagram_link',
			'type' => 'url',
		)
	);
	$wp_customize->add_control(
		'youtube_link', array(
			'label' => __('Youtube Link', 'theme'),
			'section' => 'theme_social_links',
			'settings' => 'youtube_link',
			'type' => 'url',
		)
	);
	/*
	 * social links end
	 */

	/*
	 * Address Info Start
	 */
	$wp_customize->add_section( 'theme_contact_info' , array(
		'title'      => __( 'Contact Info', 'theme' ),
		'priority'   => 30,
	) );


	// Office street address
	$wp_customize->add_setting( 'place' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'place', array(
			'label' => __('Place Name', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'place',
			'type' => 'text',
		)
	);

	// Office street address
	$wp_customize->add_setting( 'office_street_address' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_street_address', array(
			'label' => __('Office Street Address', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_street_address',
			'type' => 'text',
		)
	);

	// Office city
	$wp_customize->add_setting( 'office_city' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_city', array(
			'label' => __('Office City', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_city',
			'type' => 'text',
		)
	);
	// Office state
	$wp_customize->add_setting( 'office_state' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_state', array(
			'label' => __('Office State', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_state',
			'type' => 'text',
		)
	);

	// Office postcode
	$wp_customize->add_setting( 'office_postcode' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_postcode', array(
			'label' => __('Office Postcode', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_postcode',
			'type' => 'text',
		)
	);

	// Office Country
	$wp_customize->add_setting( 'office_country' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'office_country', array(
			'label' => __('Office Country', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'office_country',
			'type' => 'text',
		)
	);

	// Contact Phone
	$wp_customize->add_setting( 'contact_phone' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_phone', array(
			'label' => __('Contact Phone', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_phone',
			'type' => 'text',
		)
	);

	// Contact Email
	$wp_customize->add_setting( 'contact_email' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_email', array(
			'label' => __('Contact Email', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_email',
			'type' => 'text',
		)
	);

	// Contact Email
	$wp_customize->add_setting( 'contact_email' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_email', array(
			'label' => __('Contact Email', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_email',
			'type' => 'text',
		)
	);

	// Contact Email
	$wp_customize->add_setting( 'contact_shortcode' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'contact_shortcode', array(
			'label' => __('Contact Shortcode', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'contact_shortcode',
			'type' => 'text',
		)
	);


	// Copyright Details
	$wp_customize->add_setting( 'copyright_details' , array(
		'default' => '',
	) );
	$wp_customize->add_control(
		'copyright_details', array(
			'label' => __('Copyright Details', 'theme'),
			'section' => 'theme_contact_info',
			'settings' => 'copyright_details',
			'type' => 'text',
		)
	);

		// Copyright Details in brackets
		$wp_customize->add_setting( 'copyright_after' , array(
			'default' => '',
		) );
		$wp_customize->add_control(
			'copyright_after', array(
				'label' => __('Copyright Details in Brackets', 'theme'),
				'section' => 'theme_contact_info',
				'settings' => 'copyright_after',
				'type' => 'text',
			)
		);
}
add_action( 'customize_register', 'theme_customize_register' );

// Widgets


function footer_widgets_init_1() {
	register_sidebar( array(
		'name'          => __( 'Footer Menu', 'theme' ),
		'id'            => 'footer-services-2',
		'description'   => __( 'Appears on the footer under services', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-services-widget">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'footer_widgets_init_1' );

function footer_widgets_init_2() {
	register_sidebar( array(
		'name'          => __( 'Footer Icons', 'theme' ),
		'id'            => 'footer-services-3',
		'description'   => __( 'Appears on the footer under services', 'theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="footer-services-widget-logos">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'footer_widgets_init_2' );

// Options page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
//	acf_add_options_sub_page(array(
//		'page_title' 	=> 'Theme Header Settings',
//		'menu_title'	=> 'Header',
//		'parent_slug'	=> 'theme-general-settings',
//	));
//
//	acf_add_options_sub_page(array(
//		'page_title' 	=> 'Theme Footer Settings',
//		'menu_title'	=> 'Footer',
//		'parent_slug'	=> 'theme-general-settings',
//	));
	
}