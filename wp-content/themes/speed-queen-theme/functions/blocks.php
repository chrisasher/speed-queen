<?php
// Blocks

function oink_theme_setup() {
   
    // Add Align Wide and Full Width options to blocks.
    // This is done by adding the corresponding classname to the block’s wrapper.
    add_theme_support( 'align-wide' );

    // Stop users picking their own colours in the block editor
    add_theme_support( 'disable-custom-colors' );

    // Stop users from changing font sizes
    add_theme_support('disable-custom-font-sizes');

    // Custom Editor Colours
    add_theme_support( 'editor-color-palette', array(
        // array(
        //     'name' => __( 'Colour 1', 'oink' ),
        //     'slug' => 'bg-colour-1',
        //     'color' => '#26193b',
        // ),
        // array(
        //     'name' => __( 'Colour 2', 'oink' ),
        //     'slug' => 'bg-colour-2',
        //     'color' => '#f4b014',
        // ),
        array(
            'name' => __( 'Black', 'oink' ),
            'slug' => 'bg-black',
            'color' => '#000000',
        ),
        array(
            'name' => __( 'White', 'oink' ),
            'slug' => 'bg-white',
            'color' => '#ffffff',
        ),
        array(
            'name' => __( 'Grey', 'oink' ),
            'slug' => 'bg-grey',
            'color' => '#AABBC6',
        ),
        array(
            'name' => __( 'Orange', 'oink' ),
            'slug' => 'bg-orange',
            'color' => '#FCAD40',
        ),
        array(
            'name' => __( 'Blue', 'oink' ),
            'slug' => 'bg-blue',
            'color' => '#0080D2',
        ),


    ) );

    // Add a stylesheet for editing in the Dashboard
    add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
    add_editor_style( 'dashboard.css' );
}

add_action( 'after_setup_theme', 'oink_theme_setup' );


 

//Remove Gutenberg Styling from the frontend
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}

add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );




// Add Reusable blocks to menu
add_action( 'admin_menu', 'linked_url' );
function linked_url() {
add_menu_page( 'linked_url', 'Reusable Blocks', 'read', 'edit.php?post_type=wp_block', '', 'dashicons-editor-table', 22 );
}

add_action( 'admin_menu' , 'linkedurl_function' );
function linkedurl_function() {
global $menu;
$menu[1][2] = "/wp-admin/edit.php?post_type=wp_block";
}

// Register ACF Blocks
function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'Header Banner',
        'title'             => __('Header Banner'),
        'description'       => __('A custom header banner block.'),
        'render_template'   => 'partials/blocks/header-banner.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'Hero', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Featured Products',
        'title'             => __('Featured Products'),
        'description'       => __('A custom header banner block.'),
        'render_template'   => 'partials/blocks/product-carousel.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'featured', 'home' ),
    ));


    acf_register_block_type(array(
        'name'              => 'Two column section',
        'title'             => __('Two column section'),
        'description'       => __('A two column block.'),
        'render_template'   => 'partials/blocks/two-column.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'columns', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Text and map block',
        'title'             => __('Text and map block'),
        'description'       => __('A two column block.'),
        'render_template'   => 'partials/blocks/text-map.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'map', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Enquire Now',
        'title'             => __('Enquire Now'),
        'description'       => __('Enquire Now block.'),
        'render_template'   => 'partials/blocks/contact-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'contact', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Centered Content',
        'title'             => __('Centered Content'),
        'description'       => __('Centered Content block.'),
        'render_template'   => 'partials/blocks/centered-page-title.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Left Content',
        'title'             => __('Left Content'),
        'description'       => __('Left Content block.'),
        'render_template'   => 'partials/blocks/left-aligned-title.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Video Content',
        'title'             => __('Video Content'),
        'description'       => __('Video Content block.'),
        'render_template'   => 'partials/blocks/video-content.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'News Style',
        'title'             => __('News Style'),
        'description'       => __('News Style block.'),
        'render_template'   => 'partials/blocks/news-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Gallery',
        'title'             => __('Gallery'),
        'description'       => __('Gallery block.'),
        'render_template'   => 'partials/blocks/full-width-gallery.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Faq',
        'title'             => __('Faq'),
        'description'       => __('Faq block.'),
        'render_template'   => 'partials/blocks/faq.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Finance Options',
        'title'             => __('Finance Options'),
        'description'       => __('Finance Options block.'),
        'render_template'   => 'partials/blocks/column-image.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Full Screen Video',
        'title'             => __('Full Screen Video'),
        'description'       => __('Full Screen Video block.'),
        'render_template'   => 'partials/blocks/full-page-video.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Single Column',
        'title'             => __('Single Column'),
        'description'       => __('Single Column block.'),
        'render_template'   => 'partials/blocks/single-col.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Grid Image Slider',
        'title'             => __('Grid Image Slider'),
        'description'       => __('Grid Image Slider block.'),
        'render_template'   => 'partials/blocks/grid-image-slider.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'page title', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Contact Page Block',
        'title'             => __('Contact Page'),
        'description'       => __('Contact Page block.'),
        'render_template'   => 'partials/blocks/contact-page-block.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'contact us', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Contact Page Details',
        'title'             => __('Contact Page Details'),
        'description'       => __('Contact Page Details'),
        'render_template'   => 'partials/blocks/contact-details.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'contact us', 'hero' ),
    ));

    acf_register_block_type(array(
        'name'              => 'Our Markets Post Type',
        'title'             => __('Our Markets Post Type'),
        'description'       => __('Our Markets Post Type'),
        'render_template'   => 'partials/blocks/markets-cpt.php',
        'category'          => 'formatting',
        'icon'              => '<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path class="st0" d="M24,0.1c0,0-3.9-0.8-8.3,3c-1.1-0.4-2.4-0.7-3.7-0.7c-1.3,0-2.5,0.2-3.7,0.7C3.9-0.7,0,0.1,0,0.1c0.1,4.3,1.7,6.7,2.7,7.8c-0.9,1.6-1.4,3.4-1.4,5.4C1.3,19.2,6.1,24,12,24s10.7-4.8,10.7-10.8c0-2-0.5-3.8-1.4-5.4C22.3,6.7,23.9,4.4,24,0.1z M12,20.4c-2.7,0-4.9-1.6-4.9-3.6s2.2-3.6,4.9-3.6c2.7,0,4.9,1.6,4.9,3.6S14.7,20.4,12,20.4z"/><ellipse class="st0" cx="10.3" cy="16.9" rx="1.3" ry="1.3"/><ellipse class="st0" cx="13.7" cy="16.9" rx="1.3" ry="1.3"/></svg>',
        'keywords'          => array( 'contact us', 'hero' ),
    ));
}

// Check if function exists and hook into setup.
if(function_exists('acf_register_block_type')) {
    add_action('acf/init', 'register_acf_block_types');
}