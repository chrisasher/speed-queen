<?php
/**
 * Enqueues scripts and styles.
 *
 * @since Twenty Sixteen 1.0
 */
function theme_scripts() {
//	wp_enqueue_script( 'masonry-js',
//		get_stylesheet_directory_uri().'/plugs/masonry.js',
//		array( 'jquery' )
//	);
//	wp_enqueue_script( 'fancybox-js',
//		get_stylesheet_directory_uri().'/plugs/fancybox/jquery.fancybox.js',
//		array( 'jquery' )
//	);

//	wp_enqueue_script( 'theme-nav', get_template_directory_uri() . '/plugs/navbar.js', array('jquery'));

//	wp_enqueue_style('fancybox-css',get_stylesheet_directory_uri().'/plugs/fancybox/jquery.fancybox.css');
//	wp_enqueue_style('woo-small-css',get_home_url().'/wp2016/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.css', array(),'','all and (max-width:800px)');
	
	wp_enqueue_script( 'font-awesome', 'https://use.fontawesome.com/releases/v5.0.8/js/all.js');
	
	$version = '23';

		wp_enqueue_script( 'slick-js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array('jquery'), true);
		wp_enqueue_style( 'slicks-css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css');
		wp_enqueue_style( 'slicks-theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css');
	
	wp_enqueue_script( 'modal-video', get_template_directory_uri() . '/plugs/modal-video/js/jquery-modal-video.min.js', array('jquery'), $version, true);

	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/dist/js/main.js', array('jquery'), $version, true);

	wp_enqueue_style( 'oswald', 'https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap' );

	wp_enqueue_style( 'modal-css', get_template_directory_uri() . '/plugs/modal-video/css/modal-video.min.css');

	wp_enqueue_style( 'montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800;900&display=swap' );

	wp_enqueue_style( 'fontawesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css' );
	
	wp_enqueue_style( 'style', get_template_directory_uri() . '/dist/css/style.css?v='.$version );

}
add_action( 'wp_enqueue_scripts', 'theme_scripts' );