<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<header>
	<?php if(is_user_logged_in()): $addClass = 'push-down'; endif; ?>
	<div class="navigation-holder flex justify-between items-center py-2 px-4">
		<div class="first-parent flex">
			<a id="logo" class="logo-visibility" href="<?php echo get_home_url(); ?>">
				<img class="site-logo" src="<?php if (get_theme_mod( 'logo_image' )) : echo get_theme_mod('logo_image'); else: echo get_template_directory_uri().'/img/logo.png'; endif; ?>" alt="">
			</a>
			<div class="hidden lg:block search">
				<?php get_search_form(); ?>
			</div>
		</div>
		<div class="second-parent flex items-center <?php echo $addClass ?>">

			<div class="hidden lg:block">

			<?php
				$args  = array(
					'menu'            => '',
					'container'       => 'nav',
					'container_class' => 'desktop-menu',
					'theme_location' => 'primary',
				);
				wp_nav_menu($args); ?>
			</div>

			<div class="hidden lg:block phone-cta">
				<a href="tel:<?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?>"><?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?></a>
			</div>

			<div class="lg:hidden">
				<a href="tel:<?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?>" class="phone-i"><i class="fas fa-phone"></i></a>
			</div>


			<button class="menu-trigger lg:hidden"><i class="fas fa-bars"></i></button>
		</div>
	</div>

	<?php $all_categories = get_field('top_categories', 'option'); ?>
	
	<div class="service-icons hidden lg:flex lg:flex-no-wrap items-stretch lg:items-baseline justify-center">
	<?php foreach ($all_categories as $cat):
		$thumbnail_id = get_term_meta( $cat['category']->term_id, 'thumbnail_id', true );
		$image = wp_get_attachment_url( $thumbnail_id );
		$category_id = $cat['category']->term_id; ?>
		
		<a class="w-1/5 icon-holder" href="<?php echo get_term_link($cat['category']->slug, 'product_cat') ?>">
			<img src="<?php echo $image; ?>" alt="">
			<p><?php echo $cat['category']->name ?></p>
		</a>
	<?php endforeach; ?>
	</div>
</header>


<div id ="canvas-menu-overlay"></div>

<div id ="canvas-menu">

			<a id="logo" class="logo-visibility" href="<?php echo get_home_url(); ?>">
				<img class="site-logo" src="<?php if (get_theme_mod( 'logo_image' )) : echo get_theme_mod('logo_image'); else: echo get_template_directory_uri().'/img/logo.png'; endif; ?>" alt="">
			</a>

				<?php get_search_form(); ?>

			<?php
				$args  = array(
					'menu'            => '',
					'container'       => 'nav',
					'container_class' => 'mobile-menu',
					'theme_location' => 'mobile',
				);
				wp_nav_menu($args); ?>


<div class="phone-cta">
				<a href="tel:<?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?>"><?php if(get_theme_mod('contact_phone')) : echo get_theme_mod('contact_phone'); endif; ?></a>
			</div>


</div>