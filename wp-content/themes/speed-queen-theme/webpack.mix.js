const mix = require('laravel-mix');
const tailwindcss = require('tailwindcss');

mix.options({ imgLoaderOptions: { enabled: false } })
    .sourceMaps()
    .webpackConfig({ devtool: 'source-map' })
    .js('main.js', 'js')
    .sass('sass/style.scss', 'css')
    .sass('sass/dashboard.scss', '../')
    .setPublicPath('./dist')
    .options({
        processCssUrls: false,
        postCss: [ tailwindcss('./tailwind.config.js') ],
    })
    .browserSync({
        proxy: 'localhost/dev/speed-queen',
        files: [
         'sass/**/*',
         '*.php',
         '*.js',
        ]
    })