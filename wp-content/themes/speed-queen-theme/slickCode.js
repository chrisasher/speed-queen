jQuery(document).ready(function($){
	// Product Carousel
	$('.product-carousel').slick({
		dots: false,
		arrows: true,
		autoplay: true,
		infinite: false,
		autoplaySpeed: 4000,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});
})