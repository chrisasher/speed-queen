<form role="search" method="get" class="search-form flex flex-wrap flex-col" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <div>
        <input type="search" id="woocommerce-product-search-field-<?php echo isset( $index ) ? absint( $index ) : 0; ?>" class="search-field" placeholder="<?php echo esc_attr__( 'Search products&hellip;', 'woocommerce' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
        <input type="hidden" name="post_type" value="product" />
    </div>
</form>