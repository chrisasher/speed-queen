<?php
/**
 * Block Name: 2 Column BG Color Content Block
 *
 */
$heading = get_field('col_heading');
$content1 = get_field('col_content1');
$content2 = get_field('col_content2');

$field = get_field_object( 'background_colour_columns' );
$value = $field['value'];
$label = $field['choices'][ $value ];

$class = '';

if($label == 'White'){
    $class = 'layout-adjust';
}
?>
<section id="two-col" class="two-col-block flex items-center justify-center py-7 <?php echo $class; ?>" style="background-color:<?php echo esc_attr($value); ?>">
    <div class="container">
        <div class="w-full lg:w-4/5 mx-auto">
            <?php if($heading): ?>
                <div class="col-heading pb-3">
                    <?php echo $heading; ?>
                </div>
            <?php endif; ?>
            <div class="flex flex-col md:flex-row -mx-4">
                <div class="w-full md:w-1/2 px-4">
                    <?php if($content1): ?>
                        <?php echo $content1; ?>
                    <?php endif; ?>
                </div>
                <div class="w-full md:w-1/2 px-4">
                    <?php if($content2): ?>
                        <?php echo $content2; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php if($video): ?>
            <div class="video-section">
                <?php echo $video ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>