<?php

/**
 * Full Width Gallery
 */
$firstImage = get_field('first_image');
$secondImage = get_field('second_image');
$thirdImage = get_field('third_image');
$fourthImage = get_field('fourth_image');
?>

<section id="gallery" class="gallery">
    <div class="flex">
        <?php if($firstImage): ?>
        <div class="w-1/4">
            <img src="<?php echo $firstImage ?>" alt="">
        </div>
        <?php endif; ?>
        <?php if($firstImage): ?>
        <div class="w-3/5">
            <img src="<?php echo $secondImage ?>" alt="">
        </div>
        <?php endif; ?>
        <div class="w-1/4">
            <div class="flex flex-row lg:flex-col h-full">
                <img src="<?php echo $thirdImage ?>" alt="">
                <img src="<?php echo $fourthImage ?>" alt="">
            </div>
        </div>
    </div>
</section>