<?php

/**
 * Product Carousel Block Template.
 */
$heading = get_field('carousel_heading');
?>

<section id="product-carousel" class="products-carousel py-7">
    <div class="container">
        <div class="lg:w-4/5 mx-auto">
        <?php if($heading): ?>
        <h3><?php echo $heading ?></h3>
        <?php endif; ?>
            <ul class="products product-carousel">
                <?php
                // The tax query
                    $tax_query[] = array(
                        'taxonomy' => 'product_visibility',
                        'field'    => 'name',
                        'terms'    => 'featured',
                        'operator' => 'IN', // or 'NOT IN' to exclude feature products
                    );
                    $args = array(
                        'post_type' => 'product',
                        'post_status'  => 'publish',
                        'posts_per_page' => 12,
                        'tax_query' => $tax_query
                        );
                    $loop = new WP_Query( $args );
                    if ( $loop->have_posts() ) {
                        while ( $loop->have_posts() ) : $loop->the_post();
                            wc_get_template_part( 'content', 'product' );
                        endwhile;
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                ?>
            </ul>
        </div>
    </div>
</section>