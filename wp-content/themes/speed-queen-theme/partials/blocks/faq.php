<?php
/**
 * Block Name: Text Map Block
 *
 */
$heading = get_field('faq_heading');
?>
<section id="faq" class="faq lg:py-7">
    <div class="container">
        <div class="w-full lg:w-4/5 mx-auto faq-holder">
            <h3><?php echo $heading ?></h3>
            <?php if( have_rows('faqs') ): ?>
                <?php while( have_rows('faqs') ): the_row(); ?> 
                    <div class="faq-content">
                        <button class="accordion"><span><?php the_sub_field('question') ?></span> <span class="plus-minus"></span></button>
                        <div class="panel">
                            <p><?php the_sub_field('answer') ?></p>
                        </div>
                    </div>
                <?php endwhile; ?>   
            <?php endif; ?>
        </div>
    </div>
</section>