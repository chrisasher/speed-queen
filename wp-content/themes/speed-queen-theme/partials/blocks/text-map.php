<?php
/**
 * Block Name: Text Map Block
 *
 */
$heading = get_field('col_heading');
$content = get_field('content_section');
$position = get_field('column_position');
$largeButton = get_field('large_button');
?>
<section id="text-map" class="text-map-block flex items-center justify-end py-9"
style="background: url(<?php if (get_field('background_image')) : echo get_field('background_image'); endif; ?>);
background-repeat: no-repeat;
background-size: cover;
background-position: center center;
position: relative;">
    <div class="w-5/6 mx-auto">
        <div class="w-full md:w-3/4 lg:w-1/2 background-white p-4 lg:p-7 <?php echo $position ?>">
            <div class="map-content mb-5">
                <?php if($content): ?>
                    <?php echo $content; ?>
                <?php endif; ?>
            </div>
            <!-- Button section -->
            <?php if( have_rows('button_section') ): ?>
                <?php while( have_rows('button_section') ): the_row(); 
                    // Get sub field values.
                    $text = get_sub_field('button_text');
                    $link = get_sub_field('button_link');
                    ?>
                    <div class="content">
                        <a class="<?php echo $largeButton ?>" href="<?php echo esc_url( $link ); ?>"><?php echo esc_attr( $text ); ?></a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>