<?php
/**
 * Block Name: Text Map Block
 *
 */
$content = get_field('general_content');
$image = get_field('general_image');
?>
<section id="general-col" class="general-col justify-center items-center lg:py-7">
    <div class="container">
        <div class="w-full lg:w-5/6 mx-auto flex flex-col md:flex-row">
            <div class="w-full md:w-2/5">
                <?php echo $content ?>
            </div>
            <div class="w-full md:w-1/2 ml-auto">
                <img class="mx-auto" src="<?php echo $image ?>" alt="">
            </div>
        </div>
    </div>
</section>