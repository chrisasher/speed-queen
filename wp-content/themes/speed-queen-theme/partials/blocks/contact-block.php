<?php
/**
 * Block Name: Enquire Now Block
 *
 */
$heading = get_field('form_heading');
$content = get_field('form_shortcode');
?>
<section id="enquire-now" class="enquire-now-block flex items-center justify-center py-7">

    <div class="container">
        <div class="form-section-content mx-auto w-full lg:w-5/6">
            <?php if($heading): ?>
                <h3><?php echo $heading ?></h3>
            <?php endif; ?>
            <?php if($content): ?>
                <?php echo $content ?>
            <?php endif; ?>
        </div>
    </div>
</section>