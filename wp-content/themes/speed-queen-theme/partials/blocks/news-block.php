<?php

/**
 * News block
 */
$heading = get_field('news_heading');

?>

<section id="news-block" class="news-block py-7">
    <div class="container">
        <div class="w-full lg:w-4/5 mx-auto">
            <?php if($heading): ?>
                <div class="pb-2 lg:pb-3">
                    <h3><?php echo $heading ?></h3>
                </div>
            <?php endif; ?>
            <?php
                $loop = new WP_Query( array(
                    'post_type' => 'post' ,
                    'orderby' => 'date' ,
                    'order' => 'DESC' ,
                    'posts_per_page' => 2
                )
            );
            ?>
            <div class="flex">
                <?php $featured_img_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="w-full lg:w-1/2 news-block-post pb-4 lg:pb-0">
                    <?php the_post_thumbnail(); ?>
                    <p class="news-block-title uppercase mb-3 lg:pt-4"><?php echo get_the_title(); ?></p>
                    <div class="excerpt">
                        <?php the_excerpt(); ?>
                    </div>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>