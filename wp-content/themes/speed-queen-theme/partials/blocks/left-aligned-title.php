<?php

/**
 * Left Aligned Title Block.
 */
$heading = get_field('left_heading');
$content = get_field('left_content');
?>

<section id="left-page-title" class="left-page-title py-8">
    <div class="container">
        <div class="w-full lg:w-3/5 mx-auto text-left">
            <?php if($heading): ?>
                <h2><?php echo $heading ?></h2>
            <?php endif; ?>
            <div class="left-content">
                <?php if($content): ?>
                    <p><?php echo $content ?></p>
                <?php endif; ?>
            </div>
            <!-- Button section -->
            <?php if( have_rows('left_button_section') ): ?>
                <?php while( have_rows('left_button_section') ): the_row(); 
                    // Get sub field values.
                    $text = get_sub_field('left_button_text');
                    $link = get_sub_field('left_button_link');
                    ?>
                    <div class="content">
                        <a class="blue-button-large" href="<?php echo esc_url( $link ); ?>"><?php echo esc_attr( $text ); ?></a>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>