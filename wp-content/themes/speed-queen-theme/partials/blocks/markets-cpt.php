<?php

/**
 * Our Markets Block
 */
?>

<section id="markets-block-block" class="markets-block pb-6 lg:pb-8">
    <div class="container">
        <div class="w-full lg:w-4/5 mx-auto">
            <?php if($heading): ?>
                <div class="py-2 lg:py-3">
                    <h3><?php echo $heading ?></h3>
                </div>
            <?php endif; ?>
            <?php
                $loop = new WP_Query( array(
                    'post_type' => 'our_markets' ,
                    'orderby' => 'date' ,
                    'order' => 'DESC' ,
                    'posts_per_page' => -1
                )
            );
            ?>
            <div class="flex flex-wrap -mx-5">
                <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <div class="w-full md:w-1/2 lg:w-1/3 pb-7 px-5">
                    <?php 
                    if(has_post_thumbnail()) :
                    echo '<a href="' . get_permalink() . '">';
                    the_post_thumbnail('markets-thumb', array( 'class' => 'mb-3' ));
                    echo '</a>';
                    endif; ?>
                    <h3 class="uppercase"><a href="<?php the_permalink();?>"><?php echo get_the_title(); ?></a></h3>
                    <div class="excerpt"> 
                        <?php the_excerpt(); ?>
                    </div>
                    <?php if(get_theme_mod('contact_phone')) : ?>
                    <!-- <a href="tel:<?php echo get_theme_mod('contact_phone');?>" class="orange-button"><?php echo get_theme_mod('contact_phone');?></a> -->
                    <a href="<?php the_permalink();?>" class="orange-button">Read More</a>
                    <?php endif; ?>
                </div>
                <?php endwhile; wp_reset_query(); ?>
            </div>
        </div>
    </div>
</section>