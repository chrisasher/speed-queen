<?php
/**
 * Single column layout
 */
$content = get_field('content');
?>

<section id="single-column" class="single-column py-7">
    <div class="container">
        <div class="w-full lg:w-3/5 mx-auto text-left">
            <div class="content">
                <?php if($content): ?>
                    <p><?php echo $content ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>