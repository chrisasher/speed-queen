<?php
/**
 * Block Name: Contact page block
 *
 */
$heading = get_field('heading');


?>
<section id="contact-details" class="contact-details flex items-center justify-center py-8" style="background-color: <?php echo get_field('background_color') ?>">

    <div class="container">
        <div class="form-section-content mx-auto w-full lg:w-5/6">
            <h3><?php echo get_field('section_one_heading') ?></h3>
            <div class="flex flex-wrap -mx-4">
                <div class="w-full md:w-1/2 px-4">
                    <?php if( have_rows('contact_details_section_one') ): ?>
                        <?php while( have_rows('contact_details_section_one') ): the_row(); ?>
                            <?php 
                            $target = get_sub_field('new_tab');

                            if($target == 'yes'){
                                $open = "target='_blank'";
                            } else {
                                $open = "";
                            } ?>
                            <p><span><?php the_sub_field('label') ?> </span><a <?php echo $open ?> href="<?php echo get_sub_field('email') ?>"><?php the_sub_field('link_label') ?></a></p>
                        <?php endwhile; ?>   
                    <?php endif; ?>
                </div>
                <div class="w-full md:w-1/2 px-4">
                    <?php if( have_rows('contact_details_section_two') ): ?>
                        <?php while( have_rows('contact_details_section_two') ): the_row(); ?> 
                            <p><span><?php the_sub_field('label') ?> </span><a href="<?php echo get_sub_field('email') ?>"><?php the_sub_field('link_label') ?></a></p>
                        <?php endwhile; ?>   
                    <?php endif; ?>
                </div>
                <?php if( have_rows('contact_details_section_three') ): ?>
                <div class="w-full pt-4 px-4">
                    <h3><?php echo get_field('section_three_heading') ?></h3>
                        <?php while( have_rows('contact_details_section_three') ): the_row(); ?> 
                            <p><span><?php the_sub_field('label') ?> </span><a href="<?php echo get_sub_field('email') ?>"><?php the_sub_field('link_label') ?></a></p>
                        <?php endwhile; ?>   
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

</section>