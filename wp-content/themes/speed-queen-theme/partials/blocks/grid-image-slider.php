<?php

/**
 * Grid Image Slider
 */
$heading = get_field('carousel_heading');
?>

<section id="grid-image-carousel" class="grid-image-carousel lg:pb-7">
    <div class="container">
        <div class="lg:w-3/5 mx-auto">
        <?php if($heading): ?>
        <h3><?php echo $heading ?></h3>
        <?php endif; ?>
        <?php if( have_rows('images') ): ?>
            <div class="image-carousel">
                <?php while( have_rows('images') ): the_row(); ?> 
                    <img src="<?php the_sub_field('image') ?>" alt="">
                <?php endwhile; ?>
            </div>   
        <?php endif; ?>
        </div>
    </div>
</section>