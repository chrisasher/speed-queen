<?php
/**
 * Block Name: Contact page block
 *
 */
$heading = get_field('form_heading');
$headingType = get_field('heading_type');
$formCode = get_field('contact_form_shortcode');
?>
<section id="contact-us-page" class="enquire-now-block flex items-center justify-center py-7 adrian">
    <div class="container">
        <div class="form-section-content mx-auto w-full lg:w-5/6">
            <?php if($heading): ?>
                <?php if($headingType == 'Heading 1') : ?>
                    <h1><?php echo $heading ?></h1>
                <?php elseif($headingType == 'Heading 2') : ?>
                    <h2><?php echo $heading ?></h2>
                <?php else : ?>
                    <h3><?php echo $heading ?></h3>
                <?php endif; ?>
            <?php endif; ?>
            <?php if($formCode): ?>
                <?php echo do_shortcode($formCode); ?>
            <?php endif; ?>
        </div>
    </div>

</section>