<?php
/**
 * Block Name: Full Screen Video
 *
 */
$heading = get_field('heading_text');
$icon = get_field('icon_image');
$videoID = get_field('video_id');
?>
<section id="full-screen" class="full-screen flex items-center justify-center" 
style="background: url(<?php if (get_field('hero_background')) : echo get_field('hero_background'); else: echo get_template_directory_uri().'/img/header.jpg'; endif; ?>);
background-repeat: no-repeat;
background-size: cover;
background-position: center center;
position: relative;">
    <div class="container">
        <div class="play-button-block">
            <?php if($icon): ?>
                <img class="block mx-auto play-button cursor-pointer" data-video-id="<?php echo $videoID; ?>" src="<?php echo $icon; ?>" alt="play-video">
            <?php endif; ?>
        </div>
        <?php if($heading): ?>
            <div class="video-heading lg:py-3 text-center">
                <?php echo $heading; ?>
            </div>
        <?php endif; ?>
    </div>
</section>