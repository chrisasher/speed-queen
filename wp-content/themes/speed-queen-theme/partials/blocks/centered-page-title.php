<?php

/**
 * Centered Page Title Block.
 */
$heading = get_field('centered_heading');
$content = get_field('centered_content');
$button = get_field('centered_content');
$headingType = get_field('heading_type');
?>

<section id="centered-page-title" class="centered-page-title py-7">
    <div class="container">
        <div class="w-full lg:w-3/5 mx-auto text-center">
            <?php if($heading): ?>
                <?php if($headingType == 'h1') : ?>
                    <h1><?php echo $heading ?></h1>
                <?php else : ?>
                    <h2><?php echo $heading ?></h2>
                <?php endif; ?>
            <?php endif; ?>
            <div class="centered-content">
                <?php if($content): ?>
                    <p><?php echo $content ?></p>
                <?php endif; ?>
            </div>
            <!-- Button section -->
            <?php if( have_rows('centered_button_section') ): ?>
                <?php while( have_rows('centered_button_section') ): the_row(); 
                    // Get sub field values.
                    $text = get_sub_field('centered_button_text');
                    $link = get_sub_field('centered_button_link');
                    ?>
                    <?php if($link): ?>
                        <div class="content">
                            <a class="blue-button-large" href="<?php echo esc_url( $link ); ?>"><?php echo esc_attr( $text ); ?></a>
                        </div>
                    <?php endif; ?>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</section>