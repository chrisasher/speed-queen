<?php
/**
 * Block Name: Video Content
 *
 */
$className = 'video-block';
if (!empty($block['className'])) {
	$className .= ' ' . $block['className'];
}
if (!empty($block['align'])) {
	$className .= ' align' . $block['align'];
}

$heading = get_field('video_heading');
$video = get_field('video_embed');
?>
<section id="video-block" class="<?php echo esc_attr($className); ?> flex items-center justify-center py-7" style="background-color:<?php echo esc_attr($value); ?>">
    <div class="container">
        <div class="w-full lg:w-4/5 mx-auto">
            <?php if($heading): ?>
                <div class="col-heading pb-3">
                    <?php echo $heading; ?>
                </div>
            <?php endif; ?>
            <?php if($video): ?>
            <div class="video-section">
                <?php echo $video ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>