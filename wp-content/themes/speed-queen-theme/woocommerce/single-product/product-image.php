<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

?>

<!-- I only want the slider to come up if there are gallery images otherwise it doesnt work with
one featured image on its own. -->
<?php
?>
<?php

// If there are gallery products
if ($product->get_gallery_image_ids())  {
    echo '<div class="woocommerce-product-gallery single-product-carousel popup-gallery">';
    
        // include the featured image in the carousel
        if ( has_post_thumbnail( $product->id ) ) {
            $attachment_ids[0] = get_post_thumbnail_id( $product->id );
            echo '<a href="'. wp_get_attachment_image_url($attachment_ids[0], 'full' ) .'">';
            echo $attachment = wp_get_attachment_image($attachment_ids[0], 'full' );
            echo '</a>';
        }

        // include all gallery products in the carousel
        $attachment_ids = $product->get_gallery_image_ids();
        foreach( $attachment_ids as $attachment_id ) {
            echo '<a href="'. wp_get_attachment_image_url( $attachment_id, 'full' ) .'">';
            echo $image_link = wp_get_attachment_image( $attachment_id, 'full' );
            echo '</a>';
        }

    echo '</div>';

// If there are no gallery products but a featured image    
} elseif ( has_post_thumbnail( $product->id ) ) {
    echo '<div class="popup-gallery-single relative">';
    echo '<img class="badge absolute z-10" src="'.get_field('warranty_image', get_the_ID()).'" />';
    $attachment_ids[0] = get_post_thumbnail_id( $product->id ); 
    
    global $product;

$columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters(
	'woocommerce_single_product_image_gallery_classes',
	array(
		'woocommerce-product-gallery',
		'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
		'woocommerce-product-gallery--columns-' . absint( $columns ),
		'images',
	)
);

    ?>
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">

	<figure class="woocommerce-product-gallery__wrapper">
		<?php
		if ( $product->get_image_id() ) {
			$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
		} else {
			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
			$html .= '</div>';
		}

		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped

		do_action( 'woocommerce_product_thumbnails' );
		?>
	</figure>

    <?php


    echo '</div>';

    echo '<div class="full-warranty">';
    if(get_field('warranty_pdf_optional')) {
        echo '<a href ="'.get_field('warranty_pdf_optional')['url'].'" target="_blank">';
    }
    echo '<p>'.get_field('full_warranty_text', get_the_ID()).'</p>';
        if(get_field('warranty_pdf_optional')) {
        echo '</a>';
    }

    echo '</div>';
    echo '</div>';

//If there are no gallery products or featured image then display the placehiolder
} else {
    $html  = '<div class="woocommerce-product-gallery__image--placeholder">';
    $html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
    $html .= '</div>';
}

?>