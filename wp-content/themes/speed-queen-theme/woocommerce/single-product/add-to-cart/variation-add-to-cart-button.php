<?php
/**
 * Single variation cart button
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
?>
<!-- <div class="woocommerce-variation-add-to-cart variations_button flex flex-wrap md:flex-row items-middle mt-2 md:mt-5 justify-center md:justify-start"> -->


	<button type="submit" class="btn btn-solid single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

	<!-- <?php if($product->is_purchasable() || $product->is_in_stock()) : ?>
		<a class="btn btn-solid" href="<?php echo esc_url( $product->get_permalink( $product->id ) );?>">More Info</a>
	<?php endif; ?> -->


	<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>

	<input type="hidden" name="add-to-cart" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="product_id" value="<?php echo absint( $product->get_id() ); ?>" />
	<input type="hidden" name="variation_id" class="variation_id" value="0" />
<!-- </div> -->
