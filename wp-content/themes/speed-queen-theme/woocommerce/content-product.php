<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<li <?php wc_product_class( 'flex flex-col items-center justify-center py-4 my-0 md:mb-0 text-center md:text-left relative overflow-hidden', $product ); ?>>
<div class="wrap w-full ml-auto mr-auto md:pr-0 mb-3 md:mb-0 overflow-hidden">

	<?php
	/**
	 * Hook: woocommerce_before_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item' );

	/**
	 * Hook: woocommerce_before_shop_loop_item_title.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * Hook: woocommerce_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
    // if(is_single()){
    //     echo '<h3 class="h2-tag woocommerce-loop-product__title">'.$product->name.'</h3>';
    // }else{
        // do_action( 'woocommerce_shop_loop_item_title' );
    // }

	/**
	 * Hook: woocommerce_after_shop_loop_item_title.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	// do_action( 'woocommerce_after_shop_loop_item_title' );
	?>

	<?php ?>
		<div id="product-<?php the_ID(); ?>" class="popup w-3/4 mx-auto bg-white" >
			<div class="flex flex-col -mx-3 product pt-3">
				<div class="w-1/2 px-0 relative">
					<?php
					/**
					 * Hook: woocommerce_before_single_product_summary.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					// do_action( 'woocommerce_before_shop_loop_item_title' );
					?>
				</div>

				<div class="px-0">
					<div class="summary entry-summary product-details">
						<?php
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						$warranty = get_field('warranty_period', get_the_ID());
						// do_action( 'woocommerce_shop_loop_item_title' );
						echo '<h3>' . get_the_title() . '</h3>';
						// do_action( 'woocommerce_single_product_summary' );
						?>
						<div class="warranty-period">
							<?php if($warranty): ?>
								<p><?php echo $warranty ?></p>
							<?php endif; ?>
						</div>
						<a class="orange-button button alt images" href="<?php echo esc_attr( get_permalink( $product->get_id() ) ); ?>"><?php esc_attr_e( 'View More', 'wc_quick_view' ); ?></a>
					</div>
				</div> 

			</div>
		</div>
	<?php ?>

	<?php


	/**
	 * Hook: woocommerce_after_shop_loop_item.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	do_action( 'woocommerce_after_shop_loop_item' );
	?>
	</div>

</li>
