<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;
get_header( 'shop' );
?>

<?php $cate = get_queried_object() ?>
<?php if(is_product_category()  && $cate->parent != 0 ): ?>
	<?php $class = 'shop-page'; ?>
	<script>
		jQuery('body').addClass('<?php echo $class ?>');
	</script>
<?php endif; ?>

<div class="container py-4 lg:pt-7 lg:pb-9">

	<?php 
	$term = get_queried_object();
	$contentPage = get_field('content_page', $term);
	if($contentPage) {
		$gblock = get_post( $contentPage );
		echo '<div class="pb-4">';
		echo apply_filters( 'the_content', $gblock->post_content ); 
		echo '</div>';
	} ?>

	<div class="flex flex-col-reverse md:flex-row md:flex-row-reverse md:-mx-3 justify-between">
		<?php
		/**
		 * Hook: woocommerce_before_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );
		?>

		<header class="woocommerce-products-header">
			<?php
			/**
			 * Hook: woocommerce_archive_description.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			//do_action( 'woocommerce_archive_description' );
			
			the_archive_description( '', '' ); 
			$term = get_queried_object();
			if(get_field('warranty_period', 'product_cat_' . $term->term_id)) :
			?>
				<div class="expand-hide">
					<?php the_field('warranty_period', 'product_cat_' . $term->term_id);?>
				</div>
			<?php endif;?>
		</header>

		<?php
		if ( woocommerce_product_loop() ) {
			
			echo '<div class="shop-header flex flex-row flex-wrap justify-between items-start pb-4">';
			if ( apply_filters( 'woocommerce_show_page_title', true ) ) {
				if($contentPage) {
				echo '<h2 class="woocommerce-products-header__title page-title">';
				} else {
					echo '<h1 class="woocommerce-products-header__title page-title">';
				}
				woocommerce_page_title();
				if($contentPage) {
				echo '</h2>';
				} else {
					echo '</h1>';

				}
			}
			/**
			 * Hook: woocommerce_before_shop_loop.
			 *
			 * @hooked woocommerce_output_all_notices - 10
			 * @hooked woocommerce_result_count - 20
			 * @hooked woocommerce_catalog_ordering - 30
			 */

			// echo '<div class="flex flex-row items-center">';
			do_action( 'woocommerce_before_shop_loop' );
			
			echo '</div>';

			woocommerce_product_loop_start();

			if ( wc_get_loop_prop( 'total' ) ) {
				while ( have_posts() ) {
					the_post();

					/**
					 * Hook: woocommerce_shop_loop.
					 */
					do_action( 'woocommerce_shop_loop' );

					wc_get_template_part( 'content', 'product' );
				}
			}

			woocommerce_product_loop_end();

			/**
			 * Hook: woocommerce_after_shop_loop.
			 *
			 * @hooked woocommerce_pagination - 10
			 */
			do_action( 'woocommerce_after_shop_loop' );

		get_template_part('partials/content', 'reviews');

		} else {
			/**
			 * Hook: woocommerce_no_products_found.
			 *
			 * @hooked wc_no_products_found - 10
			 */
			do_action( 'woocommerce_no_products_found' );
		}
        get_template_part('partials/content', 'faq');

		/**
		 * Hook: woocommerce_after_main_content.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );




        /**
		 * Hook: woocommerce_sidebar.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
		?>
	</div>

	<?php 
	$term = get_queried_object();
	$contentPageBelow = get_field('content_page_below', $term);
	if($contentPageBelow) {
		$gblockBelow = get_post( $contentPageBelow );
		echo '<div class="pt-6">';
		echo apply_filters( 'the_content', $gblockBelow->post_content ); 
		echo '</div>';
	} ?>

</div>

<?php get_footer( 'shop' ); ?>