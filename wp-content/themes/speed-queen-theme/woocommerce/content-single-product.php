<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;



/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
?>
<section id="content" class="py-7 md:py-8">
        <?php
        do_action( 'woocommerce_before_single_product' );
        
        if ( post_password_required() ) {
            echo get_the_password_form(); // WPCS: XSS ok.
            return;
        }
        ?>
        <div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'flex flex-col md:flex-row md:text-left', $product ); ?>>

        <div class="w-full md:w-1/2 md:px-7 relative mb-3 mt-5 md:mb-0">
            <?php
            /**
             * Hook: woocommerce_before_single_product_summary.
             *
             * @hooked woocommerce_show_product_sale_flash - 10
             * @hooked woocommerce_show_product_images - 20
             */
            do_action( 'woocommerce_before_single_product_summary' );
            ?>
        </div>

        <div class="w-full md:w-1/2">

            <div class="summary entry-summary parallax-wrapper">
                <?php
                /**
                 * Hook: woocommerce_single_product_summary.
                 *
                 * @hooked woocommerce_template_single_title - 5
                 * @hooked woocommerce_template_single_rating - 10
                 * @hooked woocommerce_template_single_price - 10
                 * @hooked woocommerce_template_single_excerpt - 20
                 * @hooked woocommerce_template_single_add_to_cart - 30
                 * @hooked woocommerce_template_single_meta - 40
                 * @hooked woocommerce_template_single_sharing - 50
                 * @hooked WC_Structured_Data::generate_product_data() - 60
                 */
                do_action( 'woocommerce_single_product_summary' );
            ?>
            <?php if(is_product()): ?>
                <div class="product-heading mb-3">
                    <?php if(get_field('product_name', get_the_ID())): ?>
                        <?php the_field('product_name', get_the_ID()) ?>
                    <?php endif; ?>
                </div>
                <?php $machine = get_field( 'machine_id_section', get_the_ID()); ?>
                <div class="machine-id flex">
                    <?php
                        $machine_id_sections = get_field( 'machine_id_section', get_the_ID());
                        if(is_array($machine_id_sections) && count($machine_id_sections)){
                            foreach($machine_id_sections as $mid){
                                echo '<span class="machine-id"><span class="'.$mid['icon'].'"></span>'.$mid['id_text'].'</span>';
                            }
                        }
                    ?>
                </div>
                <?php if($machine == ''){
                    $padding = 'pt-0';
                } else {
                    $padding = 'pt-4';
                } ?>
                </div>
                <div class="prod-description w-full lg:w-4/5 <?php echo $padding ?> mb-3">

                        <?php
        // if has a more tag
        if( strpos( $post->post_content, '<!--more-->' ) ) {
            // Fetch post content
            $content = get_post_field( 'post_content', get_the_ID() );
            // Get content parts
            $content_parts = get_extended( $content ); 
            
            ?>
            <div><?php  echo apply_filters('the_content', $content_parts['main']);?></div>
            <div><button class="readmore">read more</button></div>
            <div class="extended" style="display:none"><?php  echo apply_filters('the_content', $content_parts['extended']);?></div>

            <?php
        }
        else {
            echo the_content();
        }
        ?>

                <!-- <?php the_content( $more_link_text , $strip_teaser ); ?> 
                    <?php if(get_field('product_description', get_the_ID())): ?>
                        <?php the_field('product_description', get_the_ID()) ?>
                    <?php endif; ?> -->
                </div>

                <?php 
                    //     $content = explode(get_the_content(), $separator);
                    //     $separator = '<span id="more-316">';
                    //     print_v(get_the_content()); 
                    //   echo $content[0];
                    //   echo $content[1];
                ?>

                <!-- <div class="read-more-section w-full lg:w-4/5 mb-4">
                        <button type="button" class="collapsible flex items-center">read more<img class="ml-2" src="<?php the_field('read_more_arrow', get_the_ID()) ?>" alt=""></button>
                        <div class="read-more-content mt-2">
                            <p><?php the_field('read_more_text', get_the_ID()); ?></p>
                        </div>
                </div> -->

                <div class="buttons-section flex items-start">
                    <a class="orange-button-large hover-filled-slide-right" href="#contact-target" class="enquire">Enquire Now</a>
                    <?php if(!empty(get_field('matching_appliance_link'))): ?>
                        <a class="grey-button-large hover-filled-slide-right" href="<?php echo get_field('matching_appliance_link'); ?>"><?php echo get_field('matching_appliance_text'); ?></a>
                    <?php endif; ?>
                </div>
        <?php endif; ?>
    </div>

</div>
</div>

</section>

<?php do_action( 'woocommerce_after_single_product' ); ?>
