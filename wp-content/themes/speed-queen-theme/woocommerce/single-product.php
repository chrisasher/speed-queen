<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

 <main>
    <div class="container"> 

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
     </div>
    <div class="container">
        <?php
        /**
         * Hook: woocommerce_after_single_product_summary.
         *
         * @hooked woocommerce_output_product_data_tabs - 10
         * @hooked woocommerce_upsell_display - 15
         * @hooked woocommerce_output_related_products - 20
         */
        //do_action( 'woocommerce_after_single_product_summary' );
        ?>
        <!-- tabs for the product -->
        <?php
        
            $moreInfoCol1 = get_field('column_1');
            $moreInfoCol2 = get_field('column_2');

            $downloads = get_field('downloads');

            if($moreInfoCol2 == ''){
                $class = 'w-full';
            } else {
                $class = 'w-1/2';
            }

            if($moreInfoCol1 == '') {
                $justify = 'justify-around';
                $active = 'active';
                $padding = 'md:px-9';
            } else {
                $justify = 'justify-between';
                $active = '';
                $padding = 'px-4';
            }

        ?>

        <div class="woocommerce-tabs w-full lg:w-5/6 mx-auto">
            
            <div class="tab-links mb-5">
                <!-- <div class="tab-selection flex <?php echo $justify ?>"> -->
                <div class="tab-selection">
                <?php if($moreInfoCol1): ?>
                    <a class="tab-loc" href="#tab-1">More Information</a>
                <?php endif; ?>
                    <a class="tab-loc" href="#tab-2">Specifications</a>
                    <a class="tab-loc" href="#tab-3">Downloads</a>
                </div>
            </div>

            <div class="tab-content-section pt-3">
                <?php if($moreInfoCol1): ?>
                    <div id="tab-1" class="tab-content flex">
                        <div class="<?php echo $class ?> px-4">
                            <?php echo $moreInfoCol1 ?>
                        </div>
                        <?php if(!empty($moreInfoCol2)): ?>
                        <div class="w-1/2 px-4">
                            <?php echo $moreInfoCol2 ?>
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                    
                <!-- Specifications tab -->
                <div id="tab-2" class="tab-content flex <?php echo $active; ?>">
                    <div class="w-full <?php echo $padding; ?>">
                        <?php if( have_rows('spec_section') ): ?>
                            <?php while( have_rows('spec_section') ): the_row(); ?>
                                <div class="spec-content">
                                    <div class="title">
                                        <p><?php echo get_sub_field('section_title'); ?></p>
                                    </div>
                                    <div class="specification-details">
                                        <?php if( have_rows('specs')): ?>
                                            <div>
                                                <?php while( have_rows('specs') ): the_row(); ?>
                                                    <div class="flex w-full justify-between">
                                                        <div class="spec">
                                                            <p><?php echo get_sub_field('spec') ?></p>
                                                        </div>
                                                        <div class="value text-right">
                                                            <p><?php echo get_sub_field('value') ?></p>
                                                        </div>
                                                    </div>
                                                <?php endwhile; ?>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>   
                        <?php endif; ?>
                    </div>
                </div>
                
                <!-- Downloads Tabs -->
                <div id="tab-3" class="tab-content flex">
                    <div class="w-full <?php echo $padding ?>">
                        <?php if( have_rows('downloads') ): ?>
                            <?php while( have_rows('downloads') ): the_row(); ?>
                                
                                <div class="download">
                                    <a target="_blank" href="<?php the_sub_field('file'); ?>"><i class="fas fa-file-pdf mr-2"></i><?php the_sub_field('text'); ?></a>
                                </div>
                                    
                            <?php endwhile; ?>   
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="contact-target"></div>
        <div class="form-section-content enquire-now-block mx-auto w-full lg:w-2/5 py-4 lg:py-8">
            <h3>Enquire Now</h3>
            <?php $shortCode = get_theme_mod('contact_shortcode'); ?>
            <?php if($shortCode) : echo do_shortcode($shortCode); endif; ?>
        </div>

        <div class="ancillary-items container">
            <?php if( have_rows('ancillary_items')): ?>
                <h3 class="text-center uppercase">
                <?php if (get_field('ancillary_items_heading')) : ?>
                <?php the_field('ancillary_items_heading'); ?>
                <?php else : ?>
                Ancillary Items
                <?php endif; ?>
                </h3>
                <div class="flex flex-wrap justify-center py-6 items-baseline">
                    <?php while( have_rows('ancillary_items')): the_row(); ?>
                    <?php $post_object = get_sub_field('product_link'); ?>
                            <?php if( $post_object ): ?>
                                <?php $image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg'; ?>
                                <?php $post = $post_object; setup_postdata( $post ); ?>
                                
                                <div class="flex w-full sm:w-1/2 md:w-1/5 mx-auto flex-col justify-center ancillary-item mb-3">
                                    <a class="mx-auto w-1/2" href="<?php the_permalink(); ?>"><?php echo get_the_post_thumbnail(); ?></a>
                                    <h4 class="mx-auto text-center"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    <p class="text-center px-3"><?php echo custom_wp_trim_excerpt(get_the_content(), '50') ?></p>
                                </div>

                                <?php wp_reset_postdata(); ?>
                            <?php endif; ?>
                        </a>
                    <?php endwhile; ?>   
                </div>
            <?php endif; ?>
        </div>
    </div>
</main> 

<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
