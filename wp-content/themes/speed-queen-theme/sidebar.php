<?php

if(is_active_sidebar('widget')){
	echo '<div class="w-full md:w-1/4 lg:mr-4">';
	dynamic_sidebar( 'widget' );
	echo '</div>';

}
