<?php
get_header();

?>
<main>
	<div class="container py-7">
	<?php
	// Start the loop.
	if(have_posts()){
		while ( have_posts() ){
			the_post();

			$image = (get_the_post_thumbnail_url())? get_the_post_thumbnail_url() : get_stylesheet_directory_uri().'/imgs/image.jpg';
			$title = get_the_title();
			$id = get_the_ID();
			$bodyclass = get_body_class();

			?>
			<h1 class="uppercase"><?php the_title(); ?></h1>
			<?php the_content() ?>
			<?php
			// End of the loop.
		}
	}
	?>
	</div>
</main>
<?php 
$productIDs = get_field('related_products', get_the_ID()); 
$categoryID = get_field('related_category', get_the_ID()); 

if($productIDs || $categoryID) :?>
<section id="product-carousel" class="products-carousel related-carousel py-7">
    <div class="container">
        <div class="lg:w-4/5 mx-auto">
        <h3><?php the_title() ?> Laundry Equipment</h3>
            <ul class="products">
				<?php
                    if($categoryID) {
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => -1,
                            'post_status'  => 'publish',
                            'tax_query' => array(
                                array(
                                    'taxonomy'      => 'product_cat',
                                    'field' => 'term_id', //This is optional, as it defaults to 'term_id'
                                    'terms'         => $categoryID,
                                    'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
                                ),
                            ),
                        );
                    } elseif($productIDs) {
                        $args = array(
                            'post_type' => 'product',
                            'posts_per_page' => -1,
                            'post_status'  => 'publish',
                        );
                    } 
                    
                    $loop = new WP_Query( $args );

                    if ( $loop->have_posts() ) {
                        if($categoryID) {
                            while ( $loop->have_posts() ) : $loop->the_post();
                                wc_get_template_part( 'content', 'product' );
                            endwhile;
                        } elseif($productIDs) {
                            foreach( $productIDs as $productID ): 
                                $post_object = get_post( $productID );
                                setup_postdata( $GLOBALS['post'] =& $post_object );
                                wc_get_template_part( 'content', 'product' );
                            endforeach;
                        }
                    } else {
                        echo __( 'No products found' );
                    }
                    wp_reset_postdata();
                ?>
            </ul>
        </div>
    </div>
</section>

<?php 
	$term = get_queried_object();
	$contentPageBelow = get_field('content_page_below', $term);
	if($contentPageBelow) {
		$gblockBelow = get_post( $contentPageBelow );
		echo '<div class="container py-7">';
		echo apply_filters( 'the_content', $gblockBelow->post_content ); 
		echo '</div>';
	} ?>


<?php endif; ?>
<?php
get_footer();