/**
 * Created by chris on 12/04/2016.
 */

jQuery(document).ready(function ($) {
	$('.menu__icon').click(function () {
		$('body').toggleClass('menu_shown');

		var navLinks = $('ul#menu-main-menu li');
		navLinks.each(function (link, index) {
			console.log(index);

			if (index.hasClass('animated')) {
				index.removeClass('animated');
			}
			index.addClass('animated');
		})
	});

	// $('.navigation-holder').addClass('loaded');

	$('#tab-1').addClass('active');

	let tabLinks = $('.tab-selection a');

	if ($('body').hasClass('single-product')) {
		tabLinks[0].classList.add('active');
	}

	// Handle link clicks.
	tabLinks.click(function (event) {
		var $this = $(this);

		// Prevent default click behaviour.
		event.preventDefault();

		// Remove the active class from the active link and section.
		$('.tab-content.active, .tab-selection a.active').removeClass('active');

		// Add the active class to the current link and corresponding section.
		$this.addClass('active');
		$($this.attr('href')).addClass('active');
	});

	$(document).on('scroll', function () {
		let headerPosition = $(document).scrollTop();
		if (headerPosition > 100) {
			$('.navigation-holder').addClass('fixed-header');
		} else {
			$('.navigation-holder').removeClass('fixed-header');
		}
	});

	// Modal video
	$(".play-button").modalVideo({
		channel: 'youtube',
		youtube: {
			enablejsapi: 1,
			loop: 1,
			playlist: 0,
			rel: 0,
			showinfo: 0
		}
	});

	// Product Carousel
	$('.product-carousel').slick({
		dots: false,
		arrows: true,
		autoplay: true,
		infinite: true,
		autoplaySpeed: 4000,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	// Product Carousel
	$('.image-carousel').slick({
		dots: false,
		arrows: true,
		autoplay: true,
		infinite: false,
		autoplaySpeed: 4000,
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 520,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});

	$('.menu-trigger').click(function () {
		$('body').addClass('menu-opened');
	});


	$('.mobile-menu .sub-menu').hide();
	$(".submenu-toggle").click(function () {
		$(this).parent().children("ul").slideToggle("100");
		$(this).toggleClass("rotate-down");
	});

	$('.readmore').click(function () {
		$(this).hide();
		console.log('clickkk');
		$('.extended').slideDown()
	});



	$(document).mouseup(function (e) {
		var container = $("#canvas-menu");

		// if the target of the click isn't the container nor a descendant of the container
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			$('body').removeClass('menu-opened')
		}

	});

	$(".more-link").click(function () {
		$(".collapsed-content").toggle('slow', 'swing');
		$(".full-content").toggle('slow', 'swing');
		$("#readMore").toggle();// "read more link id"
		return false;
	});

	$('.accordion-wrap').each(function () {
		if ($(this).find('div.bapf_body').length) {
			$(this).show();
		}
	});
})

var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
	coll[i].addEventListener("click", function () {
		this.classList.toggle("active");
		var content = this.nextElementSibling;
		this.classList.toggle('flip-arrow');

		if (content.style.maxHeight) {
			content.style.maxHeight = null;
		} else {
			content.style.maxHeight = content.scrollHeight + "px";
		}
	});
}

// Vanilla JS for accordion
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[0].click();
	acc[i].addEventListener("click", function () {
		/* Toggle between adding and removing the "active" class,
		to highlight the button that controls the panel */
		this.classList.toggle("active-block");

		/* Toggle between hiding and showing the active panel */
		var panel = this.nextElementSibling;
		if (panel.style.display === "block") {
			panel.style.display = "none";
		} else {
			panel.style.display = "block";
		}
	});
}

jQuery(window).load(function ($) {

})

jQuery(window).resize(function ($) {

})

function sizetorow(divheight, target, adjustment) {
	var $ = jQuery;
	if ($(window).width() > 800) {
		var maxHeight = 0;

		$(target).css('min-height', maxHeight);
		var maxHeight = $(divheight).height() - adjustment;
		console.log(maxHeight);
		//$(target).each(function(){
		//
		//})
		setTimeout(function () {
			$(target).css('min-height', maxHeight);
		}, 600)
	}
}

function makesquare(target) {
	var $ = jQuery;
	var width = $(target).width();
	$(target).height(width);
}

(function ($) {
	$('a[href*=#]:not([href=#]):not(.tab-loc)').click(function () {
		if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
			|| location.hostname == this.hostname) {

			var target = $(this.hash),

				target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');

			if (target.length) {
				$('html,body').animate({
					scrollTop: target.offset().top - 105
				}, 500);
				return false;
			}
		}
	});
})(jQuery);